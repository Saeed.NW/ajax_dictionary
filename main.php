<?php

//====================================Functions====================================
ob_start();
session_start();
date_default_timezone_set('Asia/Tehran');

//====================================Variable====================================
global $config;

//====================================Require====================================
require_once('function.php');
require_once('config.php');
require_once('db.php');

?>