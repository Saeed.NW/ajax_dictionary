<?php require_once('main.php') ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ajax</title>
    <link rel="icon" href="Success.png">
    <script type="text/javascript" src="jquery.js"></script>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="base.css">
</head>
<body style="direction: ltr">
<div id="header-wrapper"></div>
<div id="content">
    <ul class="cols">
        <li><input id="search" placeholder="type keywords" autocomplete="off"></li>
        <li>
            <div id="preview"></div>
        </li>
    </ul>
</div>
</body>
</html>

<script>
    $(function () {
        $('#search').on('keyup', function () {
            var value = $(this).val();
            $.ajax('feed.php', {
                type: 'post',
                dataType: 'json',
                data: {
                    keyword: value
                },
                success: function (data) {
                    $('#preview').html(data.html);
                }
            })
        });
    });
</script>